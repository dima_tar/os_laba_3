Лабораторна робота №3
=====================


Before you start, make sure you have installed the following requirements:
```pip install -r requirements.txt```

## Usage

### There are two ways to run the program:

1. Run the program with the default parameters:
```python cmd/main.py```
2. Run the program with .bash script:
```bash script.bash```

## Folder structure

```data``` - results from Lab #2

```head``` - default head of all levels folders. By the program head is reinitializing every time when script started.

## Comments

Script provide logging of every step due to the task. Also, it provides the ability to change the default parameters of the program. You can change the default parameters in the ```config.py``` file.



###### Таранюк Дмитро, ПС-4
###### Telegram: @dima_tar
###### Email: taranykdm@gmail.com
