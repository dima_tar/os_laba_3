from loguru import logger
import os
import shutil
from tqdm import tqdm
import config

PATH = os.path.dirname(os.path.abspath(__file__))

# Підстановка змінних
HEAD_NAME = config.HEAD_NAME

deep_level_R1 = config.deep_level_R1

amount_of_processes_N1 = config.amount_of_processes_N1
amount_of_threads_N2 = config.amount_of_threads_N2

level_r1 = config.level_r1
level_r2 = config.level_r2

level_a1 = config.level_a1
level_a2 = config.level_a2
level_a3 = config.level_a3
level_a4 = config.level_a4

level_t1 = config.level_t1
level_t2 = config.level_t2


def make_folders_by_deep_level(path, deep_level):
    ''' Функція створює папки відповідно до глибини deep_level '''
    if os.path.exists(path):
        os.system(f"rm -rf {path}")
    os.mkdir(path)
    for i in range(1, deep_level + 1):
        path = os.path.join(path, str(i))
        if not os.path.exists(path):
            os.mkdir(path)


def get_process_results_of_lab_2(deep_level, path, head_name):
    ''' Функція повертає результати роботи процесів другої лабораторної '''
    deep_path = f'{head_name}/'
    for i in range(deep_level):
        deep_path += f'{i + 1}/'
    for res in os.listdir(os.path.join(path, 'data/processes_result')):
        shutil.copy(os.path.join(path, 'data/processes_result', res), os.path.join(path, deep_path, res))


def get_thread_results_of_lab_2(deep_level, path, head_name):
    ''' Функція повертає результати роботи потоків другої лабораторної '''
    deep_path = f'{head_name}/'
    for i in range(deep_level):
        deep_path += f'{i + 1}/'
    for res in tqdm(os.listdir(os.path.join(path, 'data/threads_result')), desc='Getting thread results',
                    leave=True):
        shutil.copy(os.path.join(path, 'data/threads_result', res), os.path.join(path, deep_path, res))


def make_rar_archive(path, deep_level_from, deep_level_to, head_name, archive_name):
    ''' Функція створює архів з файлів рівня deep_level_from у папці рівня deep_level_to '''
    deep_path_from = f'{head_name}/'
    for i in range(deep_level_from):
        deep_path_from += f'{i + 1}/'
    deep_path_to = f'{head_name}/'
    for i in range(deep_level_to):
        deep_path_to += f'{i + 1}/'
    for file in tqdm(os.listdir(os.path.join(path, deep_path_from)), desc='Making rar archives', leave=True):
        if not file.endswith('.txt'):
            continue
        os.system(
            f"rar a -r -ep {os.path.join(path, deep_path_to, archive_name)}.rar {os.path.join(path, deep_path_from, file)}"
            f" > /dev/null 2>&1")


def make_one_file_of_results(path, deep_level_from, deep_level_to, head_name, one_file_name):
    ''' Функція створює один файл з результатами роботи другої лабораторної '''
    deep_path_from = f'{head_name}/'
    for i in range(deep_level_from):
        deep_path_from += f'{i + 1}/'
    deep_path_to = f'{head_name}/'
    for i in range(deep_level_from + deep_level_to):
        deep_path_to += f'{i + 1}/'
    results = {}
    for file in tqdm(os.listdir(os.path.join(path, deep_path_from)), desc='Making one file of results', leave=True):
        if not file.endswith('.txt'):
            continue
        with open(os.path.join(path, deep_path_from, file), 'r') as f:
            for line in f:
                if 'Time:' in line:
                    results[file] = float(line.split(' ')[1].strip('s\n'))
    with open(os.path.join(path, deep_path_to, one_file_name), 'w') as f:
        for file in sorted(results, key=results.get):
            f.write(f'{file} {results[file]}\n')


def add_file_to_archive(path, deep_level_from, deep_level_to, head_name):
    ''' Функція додає єдиний файл з результатами роботи другої лабораторної до архіву '''
    deep_path_from = f'{head_name}/'
    for i in range(deep_level_from):
        deep_path_from += f'{i + 1}/'
    deep_path_to = f'{head_name}/'
    for i in range(deep_level_to):
        deep_path_to += f'{i + 1}/'
    file_to_archive = ''
    archive_name = ''
    for file in os.listdir(os.path.join(path, deep_path_from)):
        if file.endswith('.txt'):
            file_to_archive = file
    for file in os.listdir(os.path.join(path, deep_path_to)):
        if file.endswith('.rar'):
            archive_name = file
    os.system(
        f"rar a -r -ep {os.path.join(path, deep_path_to, archive_name)} {os.path.join(path, deep_path_from, file_to_archive)}"
        f" > /dev/null 2>&1")


def unrar_archive(path, deep_level_from, deep_level_to, head_name):
    '''Назва говорить сама за себе'''
    deep_path_from = f'{head_name}/'
    for i in range(deep_level_from):
        deep_path_from += f'{i + 1}/'
    deep_path_to = f'{head_name}/'
    for i in range(deep_level_to):
        deep_path_to += f'{i + 1}/'
    for file in os.listdir(os.path.join(path, deep_path_from)):
        if not file.endswith('.rar'):
            continue
        os.system(
            f"unrar x {os.path.join(path, deep_path_from, file)} {os.path.join(path, deep_path_to)} > /dev/null 2>&1")


if __name__ == '__main__':

    #  Виклик функцій та логірування

    logger.info("Initiating folders")

    logger.debug("Making folders for R1 level")
    make_folders_by_deep_level(os.path.join(PATH, HEAD_NAME), deep_level_R1)

    logger.debug("Getting process results of lab 2 to level r1")
    get_process_results_of_lab_2(level_r1, PATH, HEAD_NAME)

    logger.debug("Getting thread results of lab 2 to level r2")
    get_thread_results_of_lab_2(level_r2, PATH, HEAD_NAME)

    logger.debug("Making rar archive of r1 level and moving it to a1 level")
    make_rar_archive(PATH, level_r1, level_a1, HEAD_NAME, 'processes_archive')

    logger.debug("Making rar archive of r2 level and moving it to a2 level")
    make_rar_archive(PATH, level_r2, level_a2, HEAD_NAME, 'threads_archive')

    logger.debug("Making one file of results of r1 level and moving it to r1-t1 level")
    make_one_file_of_results(PATH, level_r1, level_t1, HEAD_NAME, 'one_file_results_of_processes.txt')

    logger.debug("Making one file of results of r2 level and moving it to r2-t2 level")
    make_one_file_of_results(PATH, level_r2, level_t2, HEAD_NAME, 'one_file_results_of_threads.txt')

    logger.debug("Adding one file of results of r1+t1 level to archive of a1 level")
    add_file_to_archive(PATH, level_r1 + level_t1, level_a1, HEAD_NAME)

    logger.debug("Adding one file of results of r2+t2 level to archive of a2 level")
    add_file_to_archive(PATH, level_r2 + level_t2, level_a2, HEAD_NAME)

    logger.debug("Unrar archive of a1 level to a3 level")
    unrar_archive(PATH, level_a1, level_a3, HEAD_NAME)

    logger.debug("Unrar archive of a2 level to a4 level")
    unrar_archive(PATH, level_a2, level_a4, HEAD_NAME)

    logger.info("Done")
